import { createRouter, createWebHistory } from "vue-router"
import FrontPage from "../views/Frontpage.vue"
import AuthPage from "../views/AuthPage.vue"
import AddEvent from "../views/AddEvent.vue"
import EventPage from "../views/EventPage.vue"
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: FrontPage,
      beforeEnter: () => console.log("before enter"),
    },
    {
      path: "/auth",
      name: "auth",
      component: AuthPage,
    },
    {
      path: "/event/:id",
      name: "event",
      component: EventPage,
    },
    {
      path: "/addEvent",
      name: "add event",
      component: AddEvent,
    },
  ],
})

export default router
