// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAuC079mNeW3whtTn1oojL7Fh9fBQAvLA0",
  authDomain: "mfd2023-3c080.firebaseapp.com",
  databaseURL:
    "https://mfd2023-3c080-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "mfd2023-3c080",
  storageBucket: "mfd2023-3c080.appspot.com",
  messagingSenderId: "56944732297",
  appId: "1:56944732297:web:d5bd2580c107cad46b7e58",
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)

export default app
